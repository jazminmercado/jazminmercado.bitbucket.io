document.getElementById('addUserBtn').addEventListener('click', function() {
    var name = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    
    var outputDiv = document.getElementById('addUserOutput');
    

    addUser(name, email);
});

function addUser(name, email) {

    const user = {
        name: name,
        email: email
    };


    const xhr = new XMLHttpRequest();

    // Configure the request
    xhr.open("POST", "https://jsonplaceholder.typicode.com/users", true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onload = function() {
        if (xhr.status === 201) {
            const response = JSON.parse(xhr.responseText);
            document.getElementById("addUserOutput").innerText = `User added successfully. ID: ${response.id}`;
        } else {
            document.getElementById("addUserOutput").innerText = "Error adding user.";
        }
    };

    xhr.onerror = function() {
        document.getElementById("addUserOutput").innerText = "Network error occurred.";
    };

    xhr.send(JSON.stringify(user));


    var outputDiv = document.getElementById('addUserOutput');
    outputDiv.innerHTML = '<p>User added:</p><p>Name: ' + name + '</p><p>Email: ' + email + '</p>';
}
