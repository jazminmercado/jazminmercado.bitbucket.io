document.getElementById("getUserBtn").addEventListener("click", function() {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "https://jsonplaceholder.typicode.com/users", true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            const users = JSON.parse(xhr.responseText);
            displayUserData(users);
        } else {
            console.error("Error retrieving user data. Status code: " + xhr.status);
            document.getElementById("userData").innerText = "Error retrieving user data.";
        }
    };
    xhr.onerror = function() {
        console.error("Network error occurred while retrieving user data.");
        document.getElementById("userData").innerText = "Network error occurred.";
    };
    xhr.send();
});

function displayUserData(users) {
    let userDataHTML = "<ul>";
    users.forEach(user => {
        userDataHTML += `<li>ID: ${user.id}, Name: ${user.name}, Email: ${user.email}</li>`;
    });
    userDataHTML += "</ul>";
    document.getElementById("userData").innerHTML = userDataHTML;
}
