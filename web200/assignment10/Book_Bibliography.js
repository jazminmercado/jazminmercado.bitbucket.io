"use strict";

function Book(title, author, year, publisher, city, state) {
    this.title = title;
    this.author = author;
    this.year = year;
    this.publisher = publisher;
    this.city = city;
    this.state = state;
}

Book.prototype.apaFormat = function() {
    return `${this.author} (${this.year}) ${this.title}. ${this.city}, ${this.state}: ${this.publisher}.`;
};

Book.prototype.mlaFormat = function() {
    return `${this.author.split(' ').reverse().join(', ')} ${this.title}. ${this.publisher}, ${this.year}.`;
};

const books = [];


function addBook() {
    const title = document.getElementById('title').value;
    const author = document.getElementById('author').value;
    const year = document.getElementById('year').value;
    const publisher = document.getElementById('publisher').value;
    const city = document.getElementById('city').value;
    const state = document.getElementById('state').value;
    const format = document.getElementById('format').value;

    const book = new Book(title, author, year, publisher, city, state);
    books.push(book);

    displayBibliography(format);
}

function displayBibliography(format) {
    const bibliographyDiv = document.getElementById('bibliography');
    bibliographyDiv.innerHTML = '';

    const sortedBooks = [...books].sort((a, b) => a.title.localeCompare(b.title));
    sortedBooks.forEach(book => {
        const formatted = format === 'apa' ? book.apaFormat() : book.mlaFormat();
        bibliographyDiv.innerHTML += `<p>${format.toUpperCase()}: ${formatted}</p>`;
    });
}


document.getElementById('addBook').addEventListener('click', addBook);