// Activity 1: Displaying all tags in the current HTML document
function displayAllTags() {
    const tags = document.getElementsByTagName("*");
    const tagsArray = Array.from(tags).map(tag => tag.tagName);
    document.getElementById("tagsOutput").innerText = tagsArray.join(", ");
  }

  // Activity 2: Displaying nodeName and nodeType for all nodes
  const nodeTypeMap = {
    1: "Element",
    2: "Attribute",
    3: "Text",
    8: "Comment",
    9: "Document",
    11: "Document Fragment"
  };

  function displayNodeInfo() {
    const nodes = document.childNodes;
    let nodeInfo = "";
    for (let i = 0; i < nodes.length; i++) {
      const nodeName = nodes[i].nodeName;
      const nodeType = nodeTypeMap[nodes[i].nodeType];
      nodeInfo += `Node Name: ${nodeName}, Node Type: ${nodeType}\n`;
    }
    document.getElementById("nodeInfoOutput").innerText = nodeInfo;
  }

  // Activity 3: Displaying window size, screen size, and location information
  function displayWindowInfo() {
    const windowInfo = `
      Window Size - Width: ${window.innerWidth}, Height: ${window.innerHeight}\n
      Screen Size - Width: ${screen.width}, Height: ${screen.height}\n
      Location - ${window.location.href}
    `;
    document.getElementById("windowInfoOutput").innerText = windowInfo;
  }

  // Initialize window information display
  displayWindowInfo();

  // Update window information when window is resized
  window.addEventListener("resize", displayWindowInfo);