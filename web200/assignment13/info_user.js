"use strict";
function validateForm() {
    const firstName = document.getElementById('firstName').value;
    const lastName = document.getElementById('lastName').value;
    const address = document.getElementById('address').value;
    const city = document.getElementById('city').value;
    const region = document.getElementById('region').value;
    const postalCode = document.getElementById('postalCode').value;
    const email = document.getElementById('email').value;
    const phoneNumber = document.getElementById('phoneNumber').value;
    const dob = document.getElementById('dob').value;
  
    const phonePattern = /^[0-9]{10}$/;
    const dobPattern = /^\d{4}-\d{2}-\d{2}$/;
  
   
    const isPhoneValid = phonePattern.test(phoneNumber);
    const isDOBValid = dobPattern.test(dob);
  
    if (!isPhoneValid) {
      alert("Please enter a valid 10-digit phone number.");
      return false;
    }
  
    if (!isDOBValid) {
      alert("Please enter a valid date of birth in the format YYYY-MM-DD.");
      return false;
    }
  
    return true;
  }
  
  function handleGetRequest() {
    if (validateForm()) {
      document.getElementById('userForm').setAttribute('method', 'GET');
      document.getElementById('userForm').submit();
    }
  }
  function handlePostRequest() {
    if (validateForm()) {
      document.getElementById('userForm').setAttribute('method', 'POST');
      document.getElementById('userForm').submit();
    }
  }
  
  document.getElementById('getBtn').addEventListener('click', handleGetRequest);
  document.getElementById('postBtn').addEventListener('click', handlePostRequest);
  