function convertAge() {
    const age = parseInt(document.getElementById('age').value);
    const timeframe = document.getElementById('timeframe').value;
    
    let result;

    if (timeframe === 'M') {
        result = age * 12;
    } else if (timeframe === 'D') {
        result = age * 365;
    } else if (timeframe === 'H') {
        result = age * 365 * 24;
    } else if (timeframe === 'S') {
        result = age * 365 * 24 * 3600;
    }

    document.getElementById('result').textContent = `Approximate age: ${result}`;
}