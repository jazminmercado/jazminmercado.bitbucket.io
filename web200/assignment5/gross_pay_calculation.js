function calculateGrossPay() {
    const hours = parseFloat(document.getElementById('hours').value);
    const rate = parseFloat(document.getElementById('rate').value);
    
    let grossPay;

    if (hours <= 40) {
        grossPay = hours * rate;
    } else {
        grossPay = (40 * rate) + ((hours - 40) * rate * 1.5);
    }

    document.getElementById('result').textContent = `Gross pay: $${grossPay.toFixed(2)}`;
}
