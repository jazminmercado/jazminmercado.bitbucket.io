"use strict";

window.addEventListener("load", function() {
    document.getElementById("submitPi").addEventListener("click", calculatePi);
});

function calculatePi() {
    const value = parseInt(document.getElementById('value').value);
    const numExpressions = parseInt(document.getElementById('expressions').value);

    if (isNaN(value) || isNaN(numExpressions) || value <= 0 || numExpressions <= 0) {
        alert("Please enter valid values for the value and count.");
        return;
    }
    
    let i = 1;
    const outputList = document.getElementById('outputExpressions');
    outputList.innerHTML = ''; // Clear previous output
    
    while (i <= numExpressions) {
        const result = value * i;
        const listItem = document.createElement('li');   
        listItem.textContent = `${value} * ${i} = ${result}`;
        outputList.appendChild(listItem);
        i++;
    }
}

function calculatePi() {
    const iterations = parseInt(document.getElementById('iterations').value);

    if (isNaN(iterations) || iterations <= 0){
        alert("Please enter a valid value for iteration");
        return;
    }

    let pi = 3;
    let sign = -1;
    let divisor = 2;
    let i = 0;
    const outputList = document.getElementById('outputPi');
    outputList.innerHTML ='';

    while (i < iterations) {
        pi += (sign * 4) / (divisor * (divisor + 1) * (divisor + 2));
        sign *= -1;
        divisor += 2;
        i++;
    }

    const listItem = document.createElement('li');
    listItem.textContent = `Pi after ${iterations} iterations: ${pi}`;
    outputList.appendChild(listItem);
}
