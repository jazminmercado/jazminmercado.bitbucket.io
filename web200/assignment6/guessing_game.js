"use strict";

window.addEventListener("load", function() {
    document.getElementById("submitGame").addEventListener("click", playGuessingGame);
});

function playGuessingGame() {
    const guessInput = document.getElementById('guessInput');
    const guess = guessInput.value.trim().toLowerCase();

    if (guess !== 'h' && guess !== 'l' && guess !== 'e') {
        alert("Please enter 'h', 'l', or 'e'.");
        return;
    }
    
    let min = 0;
    let max = 100;
    let numGuesses = 0;
    let result;

    while (true) {
        const guess = Math.floor((min + max) / 2);
        result = prompt(`Is your number ${guess}? (h)igher, (l)ower, or (e)qual:`).toLowerCase();
        
        if (result === 'h') {
            min = guess + 1;
        } else if (result === 'l') {
            max = guess - 1;
        } else if (result === 'e') {
            break;
        } else {
            alert("Please enter 'h', 'l', or 'e'.");
        }
        
        numGuesses++;
    }
}