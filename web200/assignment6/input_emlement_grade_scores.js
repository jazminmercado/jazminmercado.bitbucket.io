"use strict";

window.addEventListener("load", function() {
    document.getElementById("submitgrades").addEventListener("click", calculateAverageInput);
});


function calculateAverageInput() {
    let total = 0;
    let count = 0;

    while (true) {
        const scoreInput = prompt("Enter a score (or cancel to finish):");
        
        if (scoreInput === null || scoreInput.trim() === "") {
            break; // Exit the loop if the user cancels or inputs an empty value
        }
        
        const score = parseInt(scoreInput);
        
        if (!isNaN(score)) {
            total += score;
            count++;
        } else {
            alert("Please enter a valid number.");
        }
    }

    if (count > 0) {
        const average = total / count;
        document.getElementById('averageInputResult').textContent = `Average score: ${average.toFixed(2)}`;
    } else {
        alert("No valid scores entered.");
    }
}
