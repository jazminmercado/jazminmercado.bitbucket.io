"use strict";

window.addEventListener("load", function() {
  document.getElementById("submitExpressions").addEventListener("click", generateExpressions);
});

function generateExpressions() {
    const value = parseInt(document.getElementById('value').value);
    const numExpressions = parseInt(document.getElementById('numExpressions').value);

    if (isNaN(value) || isNaN(numExpressions) || value <= 0 || numExpressions <= 0) {
        alert("Please enter valid values for the value and count.");
        return;
    }
    
    let i = 1;
    const outputList = document.getElementById('outputExpressions');
    outputList.innerHTML = ''; // Clear previous output
    
    while (i <= numExpressions) {
        const result = value * i;
        const listItem = document.createElement('li');   
        listItem.textContent = `${value} * ${i} = ${result}`;
        outputList.appendChild(listItem);
        i++;
    }
}
