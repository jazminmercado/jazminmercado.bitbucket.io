"use strict";
let total = 0;
let count = 0;

window.addEventListener("load", function() {
    document.getElementById("submit").addEventListener("click", calculateAverageGrade);
});

function calculateAverageGrade() {
    const numScores = parseInt(document.getElementById('numScores').value);

    if (isNaN(numScores) || numScores <= 0) {
        alert("Please enter a valid number of scores (greater than zero)");
        return;
    }
    
    total = 0; 
    count = 0; 

    while (count < numScores) {
        let score = parseFloat(prompt(`Enter score ${count + 1}:`));
        
        if (isNaN(score)) {
            alert(`Please enter a valid number for score ${count + 1}:`);
            continue;
        }
        
        total += score;
        count++;
    }
    
    const average = total / numScores;
    alert(`The average grade is: ${average.toFixed(2)}`);
}

