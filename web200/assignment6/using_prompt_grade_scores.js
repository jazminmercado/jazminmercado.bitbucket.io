"use strict";

let scores = [];

window.addEventListener("load", function() {
    document.getElementById("submitgrades").addEventListener("click", addScore);
});

function addScore() {
    let scorePrompt = document.getElementById("scorePrompt").scorePrompt;
    
    while (scorePrompt.trim() !== "") {
        let score = parseFloat(scorePrompt);
        if (!isNaN(score) && score > 0) {
            scores.push(score);
            document.getElementById("scorePrompt").scorePrompt = "";
        } else {
            alert("Please enter a valid positive number.");
        }
        scorePrompt = document.getElementById("scorePrompt").value;
    }

    calculateAverage();
}

function calculateAverage() {
    let total = 0;
    let count = scores.length;
    if (count > 0) {
        for (let i = 0; i < count; i++) {
            total += scores[i];
        }
        const average = total / count;
        document.getElementById('outputelementscore').textContent = `Average score: ${average.toFixed(2)}`;
    } else {
        alert("No valid scores entered.");
    }
}
