"use strict";

window.addEventListener("load", function() {
    document.getElementById("submit").addEventListener("click", calculateAverage);
});

function calculateAverage() {
    const score = parseInt(document.getElementById('score').value);
    
    if (isNaN(score) || score <= 0){
        alert("Please enter a valid score.");
        return;
    }

    let total = 0;
    for (let i = 0; i < score; i++) {
        const grade = parseFloat(prompt(`Enter grade ${i + 1}:`));
        if (isNaN(grade) || grade <= 0) {
            alert("Please enter a valid grade.");
            return;
        }
        total += grade;
    }

    const average = total / score;
    document.getElementById('output').innerHTML = `Average grade: ${average.toFixed(2)}`;
}
