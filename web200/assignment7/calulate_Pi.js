'use strict';

window.addEventListener("load", function() {
  document.getElementById("submitPi").addEventListener("click", calculatePi);
});

function calculatePi() {
    const iterations = parseInt(document.getElementById('iterations').value);

    if (isNaN(iterations) || iterations <= 0) {
        alert("Please enter valid values for the value and count.");
        return;
    }
    
    let pi = 3;
    let sign = -1;
    let denominator = 2;

    for (let i = 0; i < iterations; i++) {
      pi += 4 * sign / (denominator * (denominator + 1) * (denominator + 2));
      sign *= -1;
      denominator += 2;
    }

    document.getElementById('output').innerHTML = pi; 
}