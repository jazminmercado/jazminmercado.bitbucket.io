"use strict";

window.addEventListener("load", function() {
  document.getElementById("generate").addEventListener("click", generateExpressions);
});

function generateExpressions() {
  const value = parseInt(document.getElementById("value").value);
  const count = parseInt(document.getElementById("count").value);

  if (isNaN(value) || isNaN(count) || value <= 0 || count <= 0) {
    alert("Please enter valid values for the value and count.");
    return;
  }

  let output = "";
  for (let i = 1; i <= count; i++) {
    const result = value * i;
    output += `${value} * ${i} = ${result}<br>`;
  }

  document.getElementById("output").innerHTML = output;
}