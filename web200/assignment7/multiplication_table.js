function generateMultiplicationTable() {
    const start = parseInt(document.getElementById('start').value);
    const end = parseInt(document.getElementById('end').value);
    let output = '<table border="1"><tr><th></th>';

    // Create column labels
    for (let i = start; i <= end; i++) {
      output += `<th>${i}</th>`;
    }
    output += '</tr>';

    // Create table rows and data
    for (let i = start; i <= end; i++) {
      output += `<tr><th>${i}</th>`;
      for (let j = start; j <= end; j++) {
        output += `<td>${i * j}</td>`;
      }
      output += '</tr>';
    }

    output += '</table>';
    document.getElementById('output').innerHTML = output;
  }