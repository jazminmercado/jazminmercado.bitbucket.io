function generateMultiplicationTable() {
  const start = parseInt(document.getElementById('start').value);
  const end = parseInt(document.getElementById('end').value);
  
  // Create an array to hold the table values
  let tableValues = [];

  // Create column labels
  let headerRow = [''];
  for (let i = start; i <= end; i++) {
    headerRow.push(i);
  }
  tableValues.push(headerRow);

  // Create table rows and data
  for (let i = start; i <= end; i++) {
    let row = [i];
    for (let j = start; j <= end; j++) {
      row.push(i * j);
    }
    tableValues.push(row);
  }

  // Generate HTML table from array
  let output = '<table border="1">';
  for (let i = 0; i < tableValues.length; i++) {
    output += '<tr>';
    for (let j = 0; j < tableValues[i].length; j++) {
      if (i === 0 || j === 0) {
        output += `<th>${tableValues[i][j]}</th>`;
      } else {
        output += `<td>${tableValues[i][j]}</td>`;
      }
    }
    output += '</tr>';
  }
  output += '</table>';

  // Display the table
  document.getElementById('output').innerHTML = output;
}