"use strict";

// Function to display current date and time
function displayDateTime() {
  const currentDate = new Date();
  const dateElement = document.getElementById('data');
  const timeElement = document.getElementById('time');

  // Format date as YYYY-MM-DD
  const year = currentDate.getFullYear();
  const month = String(currentDate.getMonth() + 1).padStart(2, '0');
  const day = String(currentDate.getDate()).padStart(2, '0');
  const formattedDate = `${year}-${month}-${day}`;

  // Format time as HH:MM:SS
  const hours = String(currentDate.getHours()).padStart(2, '0');
  const minutes = String(currentDate.getMinutes()).padStart(2, '0');
  const seconds = String(currentDate.getSeconds()).padStart(2, '0');
  const formattedTime = `${hours}:${minutes}:${seconds}`;

  // Update input fields with current date and time
  dateElement.value = formattedDate;
  timeElement.value = formattedTime;
}

// Call displayDateTime function when the DOM is fully loaded
document.addEventListener('DOMContentLoaded', displayDateTime);