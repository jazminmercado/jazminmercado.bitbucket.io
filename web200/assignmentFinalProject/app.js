'use strict';

document.addEventListener('DOMContentLoaded', function () {
    const toppings = ['Pepperoni', 'Mushroom', 'Onion', 'Bell Pepper', 'Sausage', 'Olives', 'Pineapple', 'Ham', 'Bacon', 'Extra Cheese', 'Green peepers', 'Spinach'];
    const prices = {
        'Small': 6,
        'Medium': 9,
        'Large': 15,
        'Toppings': 0.5,
        'Sauce': 0,
        'Cheese': 0,
        'Wings': { 15: 25, 10: 15 },
        'Breadsticks': { 10: 10, 5: 5 },
        'DippingSauces': 0.5
    };
    const taxRate = 0.1;

    const orderForm = document.getElementById('orderForm');
    const toppingsContainer = document.getElementById('toppings');
    const orderSummary = document.getElementById('orderSummary');

    let order = {
        pizzas: [], 
        sides: {
            selectedWings: "",
            selectedBreadsticks: "",
        },
        dippingSauces: [],
        customer: {
            name: "",
            address: "",
            zip: "",
            city: "",
            state: "",
            phone: ""
        },
        comments: "",
        total: ""
    };

    toppings.forEach(topping => {
        const checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        checkbox.name = 'topping';
        checkbox.value = topping;
        toppingsContainer.appendChild(checkbox);
        const label = document.createElement('label');
        label.textContent = topping;
        toppingsContainer.appendChild(label);
        toppingsContainer.appendChild(document.createElement('br'));
    });

    document.getElementById('addPizza').addEventListener('click', function () {
        const pizzaSize = document.getElementById('pizzaSize').value;
        const selectedToppings = [...document.querySelectorAll('input[name="topping"]:checked')].map(checkbox => checkbox.value);
        const selectedWings = parseInt(document.getElementById('chickenWings').value);
        const selectedBreadsticks = parseInt(document.getElementById('breadsticks').value);
        const selectedDippingSauces = [...document.querySelectorAll('input[name="dippingSauce"]:checked')].map(checkbox => checkbox.value);
        const pizzaPrice = calculatePizzaPrice(pizzaSize, selectedToppings);
        const wingsPrice = prices['Wings'][selectedWings] || 0;
        const breadsticksPrice = prices['Breadsticks'][selectedBreadsticks] || 0;
        const dippingSaucesPrice = selectedDippingSauces.length * prices['DippingSauces'];
        const totalPrice = pizzaPrice + wingsPrice + breadsticksPrice + dippingSaucesPrice;

        const pizza = {
            size: pizzaSize,
            toppings: selectedToppings,
            wings: selectedWings,
            breadsticks: selectedBreadsticks,
            dippingSauces: selectedDippingSauces,
            price: totalPrice
        };
        order.pizzas.push(pizza);
        displayOrderSummary();
    });

    orderForm.addEventListener('submit', function (event) {
        event.preventDefault();

        order.customer.name = document.getElementById('name').value;
        order.customer.address = document.getElementById('address').value;
        order.customer.zip = document.getElementById('zip').value;
        order.customer.city = document.getElementById('city').value;
        order.customer.state = document.getElementById('state').value;
        order.customer.phone = document.getElementById('phoneNumber').value;
        order.comments = document.getElementById('comments').value;

        const totalPrice = calculateTotalPrice();
        const totalWithTax = totalPrice * (1 + taxRate);
        order.total = totalWithTax;

        document.getElementById("orderName").textContent = order.customer.name;
        document.getElementById("orderAddress").textContent = order.customer.address;
        document.getElementById("orderCity").textContent = order.customer.city;
        document.getElementById("orderState").textContent = order.customer.state;
        document.getElementById("orderZip").textContent = order.customer.zip;
        document.getElementById("orderPhone").textContent = order.customer.phone;
        document.getElementById("orderpizzaSize").textContent = order.pizzas.map(pizza => pizza.size).join(', ');
        document.getElementById("orderselectedToppings").textContent = order.pizzas.map(pizza => pizza.toppings.join(', ')).join('; ');
        document.getElementById("orderselectedWings").textContent = order.pizzas.map(pizza => pizza.wings).join(', ');
        document.getElementById("orderselectedBreadsticks").textContent = order.pizzas.map(pizza => pizza.breadsticks).join(', ');
        document.getElementById("orderselectedDippingSauces").textContent = order.pizzas.map(pizza => pizza.dippingSauces.join(', ')).join('; ');
        document.getElementById("orderPrice").textContent = `$${totalWithTax.toFixed(2)}`;
        document.getElementById("orderComments").textContent = order.comments;

        document.getElementById("orderDetails").style.display = "block";

        sendOrder(order);
    });

    function calculatePizzaPrice(size, toppings) {
        let price = prices[size];
        toppings.forEach(topping => {
            price += prices['Toppings'];
        });
        return price;
    }

    function displayOrderSummary() {
        let summaryHTML = '<h2>Order Summary</h2>';
        order.pizzas.forEach((pizza, index) => {
            summaryHTML += `<div>Pizza ${index + 1}: ${pizza.size} - $${pizza.price.toFixed(2)}</div>`;
            summaryHTML += `<div>Toppings: ${pizza.toppings.length > 0 ? pizza.toppings.join(', ') : 'None'}</div>`;
            summaryHTML += `<div>Wings: ${pizza.wings}</div>`;
            summaryHTML += `<div>Breadsticks: ${pizza.breadsticks}</div>`;
            summaryHTML += `<div>Dipping Sauces: ${pizza.dippingSauces.length > 0 ? pizza.dippingSauces.join(', ') : 'None'}</div>`;
        });
        const totalPrice = calculateTotalPrice();
        const totalWithTax = totalPrice * (1 + taxRate);
        summaryHTML += `<div>Total (before tax): $${totalPrice.toFixed(2)}</div>`;
        summaryHTML += `<div>Total (with tax): $${totalWithTax.toFixed(2)}</div>`;
        orderSummary.innerHTML = summaryHTML;
    }

    function calculateTotalPrice() {
        const totalPrice = order.pizzas.reduce((acc, pizza) => acc + pizza.price, 0);
        return totalPrice;
    }

    function sendOrder(order) {
        const xhr = new XMLHttpRequest();
        xhr.open("POST", "https://my-json-server.typicode.com/jazmin431451/jazmin-pizzaJson-orders/orders");
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(JSON.stringify(order));
        xhr.onload = function () {
            console.log(xhr.response);
        };
    }
});
